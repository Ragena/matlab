function matchingChars = getMatchingChars(i,possibleChars)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
MIN_DIAG_MULTIPLE = 0.3;
MAX_DIAG_MULTIPLE = 5.0;

MAX_AREA_CHANGE = 0.5;

MAX_WIDTH_CHANGE = 0.8;
MAX_HEIGHT_CHANGE = 0.2;

MAX_ANGLE = 12.0;

    matchingChars = [];
    for j = 1:length(possibleChars)
        if j ~= i
            I = possibleChars(i);
            J = possibleChars(j);
            
            bb_I = I.BoundingBox;
            bb_J = J.BoundingBox;
            
            x_i = bb_I(1) + bb_I(3)/2;
            y_i = bb_I(2) + bb_I(4)/2;
            x_j = bb_J(1) + bb_J(3)/2;
            y_j = bb_J(2) + bb_J(4)/2;
            
            x = x_j - x_i;
            y = y_j - y_i;
            
            distance = sqrt(x^2 + y^2);
            angle = atand(y / x);
            
            areaChange = (J.Area - I.Area) / I.Area;
            widthChange = (bb_J(3) - bb_I(3)) / bb_I(3);
            heightChange = (bb_J(4) - bb_I(4)) / bb_I(4);
            
            if (distance < sqrt(bb_I(3)^2 + bb_I(4)^2) * MAX_DIAG_MULTIPLE ... 
                    && angle < MAX_ANGLE && areaChange < MAX_AREA_CHANGE ...
                    && widthChange < MAX_WIDTH_CHANGE ...
                    && heightChange < MAX_HEIGHT_CHANGE)
                matchingChars = [matchingChars, J];
            end
        end
    end
end

