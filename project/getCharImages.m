function retImg = getCharImages(charsStruct,img)
retImg = [];
bbs = [];
for i = 1:length(charsStruct)
    I = charsStruct(i);
    bb = I.BoundingBox;
    bbs = [bbs; bb];
end
bbs = sortrows(bbs, 1);
for i = 1:size(bbs, 1)
    bb = bbs(i, :);
    im = imcrop(img, bb);
    %figure;
    %imshow(im);
    im = imresize(im, [30 20], 'nearest');
    %figure;
    %imshow(im);
    im = imresize(im, [1 600], 'nearest');
    retImg = [retImg; im];
    
end

end
