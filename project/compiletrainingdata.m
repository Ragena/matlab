img = imread('../../Pictures/Screenshot from 2017-12-22 12-50-05.png');
%img = imread('../../Pictures/Screenshot from 2017-12-22 12-04-16.png');
img_gray = rgb2gray(img);

%img_thresh = adaptthresh(img_gray);

img_binarized = imcomplement(imbinarize(img_gray));

[labeledImage, num] = bwlabel(img_binarized);
coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle'); % pseudo random color labels

chars = [];
for i = 1:num
    blob = labeledImage==i;
    rp = regionprops(blob);
    bb = rp.BoundingBox;
    char = imcrop(img_binarized, bb);
    char = imresize(char, [30 20], 'nearest');
    char = imresize(char, [1 600], 'nearest');
    chars = [chars; char];
end

f = fopen('data.txt', 'w');
for i = 1 : size(chars, 1)
    for j = 1:size(chars, 2)
        fprintf(f, "%f ", chars(i, j));
    end
    fprintf(f, "\n");
end
fclose(f);
