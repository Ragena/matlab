function possibleChar = possibleChar(rp)
%This function is a 'first pass' that does a rough check on a contour to see if it could be a char
MIN_CHAR_AREA = 40;
MAX_CHAR_AREA = 80;
MIN_CHAR_WIDTH = 2;
MIN_CHAR_HEIGHT = 8;
MIN_AR = 0.1;
MAX_AR = 1;

possibleChar = false;

bb = rp.BoundingBox;
w = bb(3);
h = bb(4);
ar = w/h;
if rp.Area > MIN_CHAR_AREA && w > MIN_CHAR_WIDTH && h > MIN_CHAR_HEIGHT ...
        && ar > MIN_AR && ar < MAX_AR
    possibleChar = true;
    %disp([rp.Area, bb])
end

