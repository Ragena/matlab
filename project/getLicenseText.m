function text = getLicenseText(chars)

[classifications, flattenedImages] = getClassesAndImages();

IDX = knnsearch(flattenedImages, chars);

text = classifications(IDX');

end
