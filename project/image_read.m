img = imread('LicPlateImages/1.png');
%img = imread('../../Pictures/Screenshot from 2017-12-22 12-04-16.png');
img_gray = rgb2gray(img);

img_thresh = adaptthresh(img_gray);

img_binarized = imcomplement(imbinarize(img_gray, img_thresh));

[labeledImage, num] = bwlabel(img_binarized);
%coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle'); % pseudo random color labels
figure;
%imshow(coloredLabels);
imshow(labeledImage, []);

possibleChars = [];

hold on;
for i = 1:num
    blob = labeledImage==i;
    rp = regionprops(blob);
    if possibleChar(rp)
        bb = rp.BoundingBox;
        rectangle('Position', bb, 'EdgeColor', 'red');
        possibleChars = [possibleChars rp];
    end
end

hold off;

%possiblePlates = [];
%indexes = [];
%matchingChars = [];
for i = 1:length(possibleChars)
    matchingChars = getMatchingChars(i, possibleChars);
    I = possibleChars(i);
    bb = I.BoundingBox;
    
    matchingChars = [matchingChars I];
    if length(matchingChars) >= 3
        chars = getCharImages(matchingChars, img_binarized);
        possiblePlate = getLicenseText(chars);
        fprintf ("%d: %s\n", i, char(possiblePlate));
    end
end

