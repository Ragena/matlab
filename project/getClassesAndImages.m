function [classifications,flattenedImages] = getClassesAndImages()
classesFile = fopen('classes.txt');
%classesFile = fopen('classifications.txt');
classifications = fscanf(classesFile, '%f');
imagesFile = fopen('data.txt');
%imagesFile = fopen('flattened_images.txt');
flattenedImages = fscanf(imagesFile, '%f', [600 180]);
flattenedImages = flattenedImages';
fclose('all');
end

