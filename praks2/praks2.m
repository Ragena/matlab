% Question 1
x = [20, 30, 40, 50, 60, 70];
y = @(x) (2 .* sind(x) + cos(x).^2)./sin(x).^2;
Q1 = y(x);

% Question 2
v = [15, 8, -6];
u = [3, -2, 6];
% a
% v ./u = [5, -4, -1]
Q2a = v ./u;
% b
% u'*v = [45, 24, -18; -30, -16, 12; 90, 48, -36]
Q2b = u' * v;
% c
% u*v' = [-7]
Q2c = u * v';

% Question 3
% This question didn't make sense to me.

% Question 4
%N = [10, 50, 100];
%symsum(((9/10).^n)./n, n, 1, 10);
n10 = [1:10];
n50 = [1:50];
n100 = [1:100];
fun = @(n) ((9/10).^n)./n;
f10 = sum(fun(n10));
f50 = sum(fun(n50));
f100 = sum(fun(n100));
