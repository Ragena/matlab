function RV = filtreq(R, C, L, w)
numerator = abs(R .*(1 - w.^2 .* L .* C));
denominator = sqrt((R - R .* w.^2 .* L .* C).^2 + (w .* L).^2);
RV = numerator ./ denominator;
end

