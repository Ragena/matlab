w = 10:10^6;

R = input('Input R: ');
L = input('Input L: ');

RV = LRFilt(R, L, w);

figure
semilogx(w, RV);

xlabel('w (rad/s)')
ylabel('Magnitude ratio')
