function y = cosTay(x)
while x > 360
    x = x - 360;
end
E = Inf;
n = 0;
y = 0;
x = deg2rad(x);
while E > 0.000001
    Sn = y;
    y = y + x .^ (2 .* n) .* (-1).^n / factorial(2 .* n);
    E = abs((y - Sn)./Sn);
    n = n + 1;
end
