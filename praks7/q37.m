w = 10:10^4;

RV1 = filtreq(200, 160e-6, 45e-3, w);
RV2 = filtreq(50, 160e-6, 45e-3, w);

figure
semilogx(w, RV1, w, RV2)

xlabel('w (rad/s)')
ylabel('Magnitude ratio')
legend('R = 200', 'R = 50')
