function RV = LRFilt(R, L, w)
RV = 1 ./ (sqrt(1 + (w .* L ./ R).^2));
end

