function K = fun(M, a, b, t)
alpha = a./b;
beta = (pi .* alpha)./2;
sigma = 6 .* M ./(t .* b.^2);
C = sqrt(tan(beta)./beta) .* (0.923 + 199 .* (1 - sin(beta)).^2) ./ cos(beta);
K = C .* sigma .* sqrt(pi .* alpha);
for i=1:length(K)
    fprintf('The stress intensity factor for a beam that is %.2fm ', b(i));
    fprintf('wide and %.2fm thick with an edge crack of %.2fm', t(i), a(i));
    fprintf(' and an applied moment of %.2fN-m is %.2f', M(i), K(i));
    fprintf(' Pa-sqrt(m)\n');
end

end

