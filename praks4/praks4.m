% Question 1
N = (1);
n = 1;
for t = 2:2:24
    for m = 1:2 * 60 / 40
        n = n * 2;
    end
    N = [N, n];
end
Q1 = N;

% Question 2
% taking t to be time in hours, I need to find the k, where
% exp(k * 13.3) = 0.5
% i.e., I am interested in:
k = log(0.5) / 13.3;
A48 = exp(k * 48);
t = 0:4:48;
Q2 = exp(k .* t);

% Question 3
data = load('q3.dat');
Q3 = fun(data(1,:), data(2,:), data(3,:), data(4,:));
