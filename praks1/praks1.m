% Question 1
% a
Q1a = sqrt(41^2 - 5.2^2) / exp(5) - 100.63;
% b
Q1b = 132^(1/3) + log(500) / 8;

% Question 2
% a
Q2a = 14.8^3 - 6.3^2 / (sqrt(13) + 5)^2;
% b
Q2b = 45 *  (288 / 9.3 - 4.6^2) - 1065 * exp(-1.5);

% Question 3
% a
Q3a = cos(7 * pi / 9) + tan(7 * pi / 15) * sind(15);
% b
Q3b = sind(80)^2 - (cosd(14) * sind(80))^2 / 0.18^(1/3);

% Question 4
fun = @(x) sin(0.6 .* x)/0.6.^2 - x .* cos(0.6 .* x) ./ 0.6;
Q4 = integral(fun, pi / 3, 3 * pi / 2);
