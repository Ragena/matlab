l = [-1 240];
w = [-1 120];
h = [-2 80];

container_vol = 12212 / 0.284;
cut_volume = 240 * 120 * 80 - container_vol;

cut_vol = conv(l, conv(w, h));

cut_vol(4) = cut_vol(4) - cut_volume; % This should be equal to zero.

answer = roots(cut_vol); % solving will give the value of t
