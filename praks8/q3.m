x = [2 5 6 8 9 13 15];
y = [7 8 10 11 12 14 15];

p = polyfit(x, y, 1);

nX = 1:16;
nY = polyval(p, nX);

figure

plot(x, y, nX, nY);
