t = 1:6;
Nb = [2000 4500 7500 15000 31000 64000];

p = polyfit(t, log(Nb), 1);

tN = 1:11;
Nn = exp(p(2)) * exp(p(1) * tN);

figure
semilogy(t, Nb, '.', tN, Nn);

figure
plot(t, Nb, '.', tN, Nn);
